<?php

use App\Http\Controllers\ReservationSettingsController;
use App\Http\Controllers\ReservationValidatorController;
use App\Models\Reservation;
use App\Models\ReservationSettings;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
})->name('index');

Route::middleware(['auth', 'verified'])->group(function() {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard', [
            'total_users' => User::count(),
            'total_reservations' => Reservation::count(),
            'reservations_today' => Reservation::whereDate('created_at', '>=', Carbon::today())->count(),
            'total_settings' => ReservationSettings::count(),
            'latest_reservations' => Reservation::with('user:id,name,email')->latest()->limit(10)->get()->toArray(),
        ]);
    })->name('dashboard');
    
    Route::prefix('reservation')->group(function() {
        Route::prefix('settings')->as('settings.')->group(function() {
            Route::get('/', [ReservationSettingsController::class, 'index'])->name('index');
            Route::post('/fetch', [ReservationSettingsController::class, 'fetch'])->name('fetch');
            Route::post('/store', [ReservationSettingsController::class, 'store'])->name('store');
        });

        Route::prefix('validator')->as('validator.')->group(function() {
            Route::get('/', [ReservationValidatorController::class, 'index'])->name('index');
            Route::post('/store', [ReservationValidatorController::class, 'store'])->name('store');
        });
    });
    
});

require __DIR__.'/auth.php';

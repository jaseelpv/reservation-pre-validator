<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReservationSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete all records first
        DB::table('reservation_settings')->delete();

        DB::table('reservation_settings')->insert([
            'id'                => 1,
            'reservation_count' => 2,
            'duration'          => 'day',
            'type'              => 'individual',
            'timezone'          => 'Asia/Kolkata',
            'created_at'        => Carbon::now(),
            'updated_at'        => Carbon::now()
        ]);
    }
}

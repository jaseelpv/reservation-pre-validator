<?php

namespace App\Http\Controllers;

use App\Models\ReservationSettings;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;

class ReservationSettingsController extends Controller
{
    public function index() {
        return Inertia::render('Settings');
    }

    public function fetch() {
        $settings = ReservationSettings::latest()->first();
        $status = 'failed';

        if($settings) {
            $status = 'success';
        }

        return response([
            'status'    => $status,
            'settings'  => $settings
        ], 201);
    }

    public function store() {
        Request::validate([
            'reservation_count' => ['required', 'numeric', 'min:1'],
            'duration' => ['required'],
            'type' => ['required'],
            'timezone' => ['required', 'max:30', 'string'],
        ]);

        $data = request()->except('_token');
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();

        $settings = ReservationSettings::insert($data);

        $status = 'error';

        if($settings) {
            $status = 'success';
        }

        return response([
            'status'    => $status
        ], 201);
    }
}

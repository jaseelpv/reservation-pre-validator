<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Models\ReservationSettings;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;

class ReservationValidatorController extends Controller
{
    public function index() {
        return Inertia::render('Validator', [
            'options' => User::select('id', 'name')->when(request('term'), function($query, $term) {
                $query->where('name', 'like', "%$term%");
            })->limit(15)->get()
        ]);
    }

    public function store() {
        Request::validate([
            'users_id' => ['required', 'array'],
            'reservation_datetime' => ['required'],
        ]);

        $errors = $messages = [];

        $data = request()->except('_token');

        $settings = ReservationSettings::latest()->first();

        if($settings->duration == 'day') {
            $date = Carbon::today()->format('Y-m-d H:i:s');
        } elseif($settings->duration == 'month') {
            $date = Carbon::today()->subDays(7)->format('Y-m-d H:i:s');
        } else {
            $date = Carbon::today()->subDays(30)->format('Y-m-d H:i:s');
        }

        
        if($settings->type == 'individual') {
            foreach($data['users_id'] as $user_id) {
                $user_name = User::where('id', $user_id)->value('name');
                $reserved_count = Reservation::where('user_id', $user_id)->whereBetween('created_at', [$date, Carbon::now()])->count();
                if($reserved_count < $settings->reservation_count) {
                    Reservation::insert([
                        'user_id' => $user_id,
                        'reservation_datetime' => $data['reservation_datetime'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);

                    $messages[] = 'Reservation successfull for '.$user_name.' on '. Carbon::parse($data['reservation_datetime'])->format('d-M-Y') .' at '. Carbon::parse($data['reservation_datetime'])->format('h:i A');
                } else {
                    $errors[] = $user_name.' has crossed the reservation limit.';
                }
            }
        } else {
            $users_count = count($data['users_id']);
            $reserved_count = Reservation::whereIn('user_id', $data['users_id'])->whereBetween('created_at', [$date, Carbon::now()])->count();
            if($reserved_count < $settings->reservation_count) {
                $remaining_reservation_count = $settings->reservation_count - $reserved_count;
                if($remaining_reservation_count >= $users_count) {
                    foreach($data['users_id'] as $user_id) {
                        $user_name = User::where('id', $user_id)->value('name');
                        Reservation::insert([
                            'user_id' => $user_id,
                            'reservation_datetime' => $data['reservation_datetime'],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ]);

                        $messages[] = 'Reservation successfull for '.$user_name.' on '. Carbon::parse($data['reservation_datetime'])->format('d-M-Y') .' at '. Carbon::parse($data['reservation_datetime'])->format('h:i A');
                    }
                } else {
                    $allowed_users = array_slice($data['users_id'], 0, $remaining_reservation_count);
                    $restricted_users = array_slice($data['users_id'], $remaining_reservation_count);

                    foreach($allowed_users as $user_id) {
                        $user_name = User::where('id', $user_id)->value('name');
                        Reservation::insert([
                            'user_id' => $user_id,
                            'reservation_datetime' => $data['reservation_datetime'],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ]);

                        $messages[] = 'Reservation successfull for '.$user_name.' on '. Carbon::parse($data['reservation_datetime'])->format('d-M-Y') .' at '. Carbon::parse($data['reservation_datetime'])->format('h:i A');
                    }

                    $user_names = null;
                    foreach($restricted_users as $user_id) {
                        $user_names .= User::where('id', $user_id)->value('name').',';
                    }
                    $errors[] = 'Reservation failed for "'.$user_names.'" as the reservation limit is exceeded for this group.';
                }
            } else {
                $errors[] = 'The reservation limit is exceeded for this group.';
            }
        }

        return response([
            'messages'    => $messages,
            'errors'    => $errors,
        ], 201);
    }
}

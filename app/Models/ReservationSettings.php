<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReservationSettings extends Model
{
    use HasFactory;

    protected $table = 'reservation_settings';

    protected $fillable = [
        'reservation_count', 'duration', 'type', 'timezone'
    ];
}
